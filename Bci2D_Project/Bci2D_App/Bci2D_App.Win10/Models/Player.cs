﻿namespace Bci2D_App.Win10.Models {
    public class Player {
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
