﻿namespace Bci2D_App.Win10.Models {
    public class Enemy {
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
    }
}
