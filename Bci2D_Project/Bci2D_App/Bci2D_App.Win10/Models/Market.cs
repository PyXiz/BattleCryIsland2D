﻿namespace Bci2D_App.Win10.Models {
    public class Market {
        public int Uid { get; set; }
        public string Name { get; set; }
        public int Lock { get; set; }
        public int Price { get; set; }
        public Item Trade { get; set; } = new Item();
        public Item Product { get; set; } = new Item();
        public bool Slot { get; set; }
    }
}
