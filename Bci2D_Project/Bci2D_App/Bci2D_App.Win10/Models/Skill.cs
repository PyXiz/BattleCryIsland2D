﻿namespace Bci2D_App.Win10.Models {
    public class Skill {
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float CoolDown { get; set; }
        public int Cost { get; set; }
        public bool HitArea { get; set; }
    }
}
