﻿using System.Windows;
using System.Windows.Controls;
using Bci2D_App.Win10.ViewModels;

namespace Bci2D_App.Win10.Views.Players {
    public partial class SkillView : UserControl {
        public SkillView() {
            InitializeComponent();
            vm = new SkillViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataSkill;
                if (form != null) {
                    form.Close();
                }
                vm.ModelSkill = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            DataContext = vm;
        }

        private readonly SkillViewModel vm;
        private SkillForm form;

        private void InitForm() {
            form = new SkillForm(vm);
            form.ShowDialog();
        }

        private void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (vm.ModelSkill != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e) {
            InitForm();
        }

        private void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelSkill = null;
            InitForm();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e) {
            vm.ModelSkill = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
