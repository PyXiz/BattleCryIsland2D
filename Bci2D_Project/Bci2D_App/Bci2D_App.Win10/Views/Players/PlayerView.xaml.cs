﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Bci2D_App.Win10.ViewModels;

namespace Bci2D_App.Win10.Views.Players {
    public partial class PlayerView : UserControl {
        public PlayerView() {
            InitializeComponent();
            vm = new PlayerViewModel();
            vm.OnReload += () => {
                LstData.ItemsSource = null;
                LstData.ItemsSource = vm.DataPlayer;
                if (form != null) {
                    form.Close();
                }
                vm.ModelPlayer = null;
                BtnEdit.Visibility = Visibility.Hidden;
                BtnReset.Visibility = Visibility.Hidden;
            };
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
            DataContext = vm;
        }

        private PlayerViewModel vm;
        private PlayerForm form;

        private async Task InitFormAsync() {
            await Task.Delay(0);
            form = new PlayerForm(vm);
            form.ShowDialog();
        }

        private async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            await Task.Delay(0);
            if (vm.ModelPlayer != null) {
                BtnEdit.Visibility = Visibility.Visible;
                BtnReset.Visibility = Visibility.Visible;
            }
        }

        private async void BtnNew_Click(object sender, RoutedEventArgs e) {
            vm.ModelPlayer = null;
            await InitFormAsync();
        }

        private async void BtnEdit_Click(object sender, RoutedEventArgs e) {
            await InitFormAsync();
        }

        private async void BtnReset_Click(object sender, RoutedEventArgs e) {
            await Task.Delay(0);
            vm.ModelPlayer = null;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnReset.Visibility = Visibility.Hidden;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
        }
    }
}
