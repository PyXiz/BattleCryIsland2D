﻿using System.Windows;
using Bci2D_App.Win10.ViewModels;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.Views.Players {
    public partial class SkillForm : Window {
        public SkillForm(SkillViewModel vm) {
            InitializeComponent();
            if (vm.ModelSkill == null) {
                vm.ModelSkill = new Skill();
                BtnSave.Visibility = Visibility.Visible;
                BtnUpdate.Visibility = Visibility.Hidden;
                BtnDelete.Visibility = Visibility.Hidden;
            } else {
                BtnSave.Visibility = Visibility.Hidden;
                BtnUpdate.Visibility = Visibility.Visible;
                BtnDelete.Visibility = Visibility.Visible;
            }
            DataContext = vm;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
