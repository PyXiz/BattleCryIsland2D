﻿using System.Windows;
using Bci2D_App.Win10.ViewModels;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.Views.Players {
    public partial class PlayerForm : Window {
        public PlayerForm(PlayerViewModel vm) {
            InitializeComponent();
            if (vm.ModelPlayer == null) {
                vm.ModelPlayer = new Player();
                BtnDelete.Visibility = Visibility.Hidden;
                BtnUpdate.Visibility = Visibility.Hidden;
                BtnSave.Visibility = Visibility.Visible;
            } else {
                BtnDelete.Visibility = Visibility.Visible;
                BtnUpdate.Visibility = Visibility.Visible;
                BtnSave.Visibility = Visibility.Hidden;
            }
            DataContext = vm;
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
