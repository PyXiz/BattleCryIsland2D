﻿using System.Windows;

namespace Bci2D_App.Win10.Views.Home {
    public partial class Dashboard : Window {
        public Dashboard() {
            InitializeComponent();
        }

        private void BtnPlayer_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Players.PlayerView());
        }

        private void BtnProfile_Click(object sender, RoutedEventArgs e) {
            // do Nothing;
        }

        private void BtnSkill_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Players.SkillView());
        }

        private void BtnEnemies_Click(object sender, RoutedEventArgs e) {
            App.ViewRouting(false);
            App.ViewRouting(true, new Stuffs.EnemyView());
        }

        private void BtnItem_Click(object sender, RoutedEventArgs e) {

        }

        private void BtnMarket_Click(object sender, RoutedEventArgs e) {

        }

        private void MnuExit_Click(object sender, RoutedEventArgs e) {
            Application.Current.Shutdown();
        }
    }
}
