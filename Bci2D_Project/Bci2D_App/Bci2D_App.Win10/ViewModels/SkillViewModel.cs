﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.ViewModels {
    public class SkillViewModel : BaseViewModel {
        public SkillViewModel() {
            dataskill = new ObservableCollection<Skill>();
            modelskill = new Skill();

            CreateCommand = new Command(async () => await CreateSkillAsync());
            UpdateCommand = new Command(async () => await UpdateSkillAsync());
            DeleteCommand = new Command(async () => await DeleteSkillAsync());
            ReadCommand = new Command(async () => await ReadSkillAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Skill> DataSkill {
            get {
                return dataskill;
            }
            set {
                SetProperty(ref dataskill, value);
            }
        }

        public Skill ModelSkill {
            get {
                return modelskill;
            }
            set {
                SetProperty(ref modelskill, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Skill> dataskill;
        private Skill modelskill;

        private async Task InitSkillAsync() {
            await Task.Run(() => {
                DataSkill = new ObservableCollection<Skill> {
                    new Skill { Uid = 1, Name = "Force Attack", Description = "Melee skill attack", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 2, Name = "Focus Strike", Description = "Melee skill attack", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 3, Name = "Fire Breath", Description = "Cast skill attack", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 4, Name = "Stun Gun", Description = "Melee skill attack", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 5, Name = "Battle Cry", Description = "Buff attack", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 6, Name = "Healing Blow", Description = "Cast heal and skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 7, Name = "Purging Dust", Description = "Cast heal regen and skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 8, Name = "Star Explode", Description = "Cast skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 9, Name = "Splash Attack", Description = "Melee skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 10, Name = "Lethal Strike", Description = "Melee skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 11, Name = "Homming Missile", Description = "Range skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 12, Name = "Final Blow", Description = "Melee skill attack area", Cost = 10, CoolDown = 10, HitArea = true},
                    new Skill { Uid = 13, Name = "Holy Protector", Description = "Summon spirit skill", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 14, Name = "Burning Soul", Description = "Cast skill attack DPS", Cost = 10, CoolDown = 10, HitArea = false},
                    new Skill { Uid = 15, Name = "EMP Mine", Description = "Range skill attack area and DPS", Cost = 10, CoolDown = 10, HitArea = true},
                };
            });
        }

        private async Task ReadSkillAsync(bool asnew = false) {
            if (asnew) {
                await InitSkillAsync();
            } else {
                OnReload?.Invoke();
            }
        }

        private async Task<Skill> ReadSkillAsync(int uid) {
            await Task.Delay(0);
            return DataSkill.Where(model => model.Uid.Equals(uid)).SingleOrDefault();
        }

        private async Task CreateSkillAsync() {
            DataSkill.Add(ModelSkill);
            await ReadSkillAsync();
        }

        private async Task UpdateSkillAsync() {
            var data = ModelSkill;
            DataSkill.Remove(ReadSkillAsync(data.Uid).Result);
            DataSkill.Add(data);
            await ReadSkillAsync();
        }

        private async Task DeleteSkillAsync() {
            DataSkill.Remove(ModelSkill);
            await ReadSkillAsync();
        }
    }
}
