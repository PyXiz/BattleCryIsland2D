﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.ViewModels {
    public class ItemViewModel : BaseViewModel {
        public ItemViewModel() {
            dataitem = new ObservableCollection<Item>();
            modelitem = new Item();

            CreateCommand = new Command(async () => await CreateItemAsync());
            UpdateCommand = new Command(async () => await UpdateItemAsync());
            DeleteCommand = new Command(async () => await DeleteItemAsync());
            ReadCommand = new Command(async () => await ReadItemAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Item> DataItem {
            get {
                return dataitem;
            }
            set {
                SetProperty(ref dataitem, value);
            }
        }

        public Item ModelItem {
            get {
                return modelitem;
            }
            set {
                SetProperty(ref modelitem, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Item> dataitem;
        private Item modelitem;

        private async Task InitItemAsync() {
            await Task.Run(() => {
                DataItem = new ObservableCollection<Item> {
                    new Item { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 1 },
                    new Item { Uid = 12, Name = "Red Elixir", Description = "Ramuan penyembuh luka hasil pertempuran, efek menambah 100 hp", Amount = 1 },
                    new Item { Uid = 13, Name = "Blue Powder", Description = "Memulihkan tenaga untuk kembali bertarung, efek menambah 100 sp", Amount = 1 },
                    new Item { Uid = 14, Name = "Green Leaf", Description = "Dapat digunakan untuk mencatat proses permainan", Amount = 1 },
                    new Item { Uid = 21, Name = "Gladius", Description = "Pedang kualitas tinggi, menambahkan 50 ATK dan mengaktifkan skill Force Attack", Amount = 1 },
                    new Item { Uid = 22, Name = "Great Sword", Description = "Pedang penghancur, menambahkan 100 ATK, 30 DEF dan mengaktifkan skill Splash Attack", Amount = 1 },
                    new Item { Uid = 31, Name = "Battle Gear", Description = "Armor besi, menambahkan 50 DEF, 100 HP, 50 SP dan mengaktifkan skill Battle Cry", Amount = 1 },
                    new Item { Uid = 41, Name = "Codex of Vitality", Description = "Instan efek menambah 50 maximum hp", Amount = 1 },
                    new Item { Uid = 42, Name = "Codex of Mentality", Description = "Instan efek menambah 50 maximum sp", Amount = 1 },
                };
            });
        }

        private async Task ReadItemAsync(bool asnew = false) {
            if (asnew) {
                await InitItemAsync();
            } else {
                OnReload?.Invoke();
            }
        }

        private async Task<Item> ReadItemAsync(int uid) {
            await Task.Delay(0);
            return DataItem.Where(model => model.Uid.Equals(uid)).SingleOrDefault();
        }

        private async Task CreateItemAsync() {
            DataItem.Add(ModelItem);
            await ReadItemAsync();
        }

        private async Task UpdateItemAsync() {
            var data = ModelItem;
            DataItem.Remove(ReadItemAsync(data.Uid).Result);
            DataItem.Add(data);
            await ReadItemAsync();
        }

        private async Task DeleteItemAsync() {
            DataItem.Remove(ModelItem);
            await ReadItemAsync();
        }
    }
}
