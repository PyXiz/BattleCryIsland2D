﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.ViewModels {
    public class PlayerViewModel : BaseViewModel {
        public PlayerViewModel() {
            dataplayer = new ObservableCollection<Player>();
            modelplayer = new Player();

            CreateCommand = new Command(async () => await CreatePlayerAsync());
            UpdateCommand = new Command(async () => await UpdatePlayerAsync());
            DeleteCommand = new Command(async () => await DeletePlayerAsync());
            ReadCommand = new Command(async () => await ReadPlayerAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Player> DataPlayer {
            get {
                return dataplayer;
            }
            set {
                SetProperty(ref dataplayer, value);
            }
        }

        public Player ModelPlayer {
            get {
                return modelplayer;
            }
            set {
                SetProperty(ref modelplayer, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Player> dataplayer;
        private Player modelplayer;

        private async Task InitPlayerAsync() {
            await Task.Run(() => {
                DataPlayer = new ObservableCollection<Player> {
                    new Player { Uid = 1, Name = "Lone Soldier", Title = "Knight", Description = "Mantan prajurit elit keluarga kerajaan yang akhirnya dikucilkan lalu diasingkan ke Battle Cry Island. Petarung jarak dekat, handal menggunakan pedang dan dilengkapi dengan extra pedang panjang untuk membubarkan musuh yang berkumpul. Memiliki daya tahan tubuh yang baik."},
                    new Player { Uid = 2, Name = "Bounty Hunter", Title = "Rogue", Description = "Petarung dari koloseum yang berhasil mendapatkan kebebasannya. Datang ke Battle Cry Island sebagai pemburu bayaran. Petarung jarak dekat, handal menggunakan pedang dan pisau. Sangat mematikan dalam pertarungan 1 lawan 1."},
                    new Player { Uid = 3, Name = "Firesoul Keeper", Title = "Wizzard", Description = "Pengendali api dari aliran pemuja matahari di utus ke Battle Cry Island untuk mencari kebenaran tentang kebangkitan raja iblis. Api yang dimilikinya dapat membakar habis para musuh yang menghadang. sangat rentan dengan serangan bergerombol."},
                    new Player { Uid = 4, Name = "Blessed Maiden", Title = "Priest", Description = "Pemimpin temple Altar of Life. Lahir dari kaum bangsawan, memiliki tekad yang kuat Datang ke Battle Cry Island untuk membasmi semua iblis yang kian berkembang. Menggunakan cahaya untuk melawan kejahatan, Hebat dalam memberikan dukungan pertempuran Dapat memanggil spirit untuk bertarung bersamanya."},
                    new Player { Uid = 5, Name = "Time Patrol", Title = "Scout", Description = "Penjelajah waktu dari dunia luar yang terdampar ke Battle Cry Island. Handal dalam pertarungan jarak jauh, menggunakan senjata laras panjang dan ranjau kejut yang ditanam untuk membubarkan gerombolan. Hebat dalam memberikan dukungan pertempuran."},
                };
            });
        }

        private async Task ReadPlayerAsync(bool asnew = false) {
            if (asnew) {
                await InitPlayerAsync();
            } else {
                OnReload?.Invoke();
            }
        }

        private async Task<Player> ReadPlayerAsync(int uid) {
            await Task.Delay(0);
            return DataPlayer.Where(model => model.Uid.Equals(uid)).SingleOrDefault();
        }

        private async Task CreatePlayerAsync() {
            DataPlayer.Add(ModelPlayer);
            await ReadPlayerAsync();
        }

        private async Task UpdatePlayerAsync() {
            var data = ModelPlayer;
            DataPlayer.Remove(ReadPlayerAsync(data.Uid).Result);
            DataPlayer.Add(data);
            await ReadPlayerAsync();
        }

        private async Task DeletePlayerAsync() {
            DataPlayer.Remove(ModelPlayer);
            await ReadPlayerAsync();
        }
    }
}
