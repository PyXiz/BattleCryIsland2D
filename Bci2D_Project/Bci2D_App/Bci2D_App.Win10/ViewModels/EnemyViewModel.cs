﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.ViewModels {
    public class EnemyViewModel : BaseViewModel {
        public EnemyViewModel() {
            dataenemy = new ObservableCollection<Enemy>();
            modelenemy = new Enemy();

            CreateCommand = new Command(async () => await CreateEnemyAsync());
            UpdateCommand = new Command(async () => await UpdateEnemyAsync());
            DeleteCommand = new Command(async () => await DeleteEnemyAsync());
            ReadCommand = new Command(async () => await ReadEnemyAsync(true));
            ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Enemy> DataEnemy {
            get {
                return dataenemy;
            }
            set {
                SetProperty(ref dataenemy, value);
            }
        }

        public Enemy ModelEnemy {
            get {
                return modelenemy;
            }
            set {
                SetProperty(ref modelenemy, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Enemy> dataenemy;
        private Enemy modelenemy;

        private async Task InitEnemyAsync() {
            await Task.Run(() => {
                DataEnemy = new ObservableCollection<Enemy> {
                    new Enemy { Uid = 1, Name = "Mob Worker", Description = "Creep pekerja", Type = 0},
                    new Enemy { Uid = 2, Name = "Mob Soldier", Description = "Creep prajurit", Type = 1},
                    new Enemy { Uid = 3, Name = "Mob Harvest", Description = "Cast skill attack", Type = 2},
                };
            });
        }

        private async Task ReadEnemyAsync(bool asnew = false) {
            if (asnew) {
                await InitEnemyAsync();
            } else {
                OnReload?.Invoke();
            }
        }

        private async Task<Enemy> ReadEnemyAsync(int uid) {
            await Task.Delay(0);
            return DataEnemy.Where(model => model.Uid.Equals(uid)).SingleOrDefault();
        }

        private async Task CreateEnemyAsync() {
            DataEnemy.Add(ModelEnemy);
            await ReadEnemyAsync();
        }

        private async Task UpdateEnemyAsync() {
            var data = ModelEnemy;
            DataEnemy.Remove(ReadEnemyAsync(data.Uid).Result);
            DataEnemy.Add(data);
            await ReadEnemyAsync();
        }

        private async Task DeleteEnemyAsync() {
            DataEnemy.Remove(ModelEnemy);
            await ReadEnemyAsync();
        }
    }
}
