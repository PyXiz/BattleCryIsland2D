﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Bci2D_App.Win10.Models;

namespace Bci2D_App.Win10.ViewModels {
    public class MarketViewModel : BaseViewModel {
        public MarketViewModel() {
            dataitem = new ObservableCollection<Item>();
            datamarket = new ObservableCollection<Market>();
            modelmarket = new Market();
            modelitem = new Item();

            CreateCommand = new Command(async () => await CreateMarketAsync());
            UpdateCommand = new Command(async () => await UpdateMarketAsync());
            DeleteCommand = new Command(async () => await DeleteMarketAsync());
            ReadCommand = new Command(async () => await ReadMarketAsync(true));
            SelectCommand = new Command(async () => await SelectItemAsync());
            FinderCommand = new Command(async () => await FinderItemAsync());

            ReadCommand.Execute(null);
            FinderCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand FinderCommand { get; set; }
        public ICommand SelectCommand { get; set; }

        public ObservableCollection<Market> DataMarket {
            get {
                return datamarket;
            }
            set {
                SetProperty(ref datamarket, value);
            }
        }

        public ObservableCollection<Item> DataItem {
            get {
                return dataitem;
            }
            set {
                SetProperty(ref dataitem, value);
            }
        }

        public Market ModelMarket {
            get {
                return modelmarket;
            }
            set {
                SetProperty(ref modelmarket, value);
                if (modelmarket != null) {
                    TradeMarket = (modelmarket.Trade != null) ? $"{modelmarket.Trade.Name} ({modelmarket.Trade.Amount})" : "";
                    ProductMarket = (modelmarket.Product != null) ? $"{modelmarket.Product.Name} ({modelmarket.Product.Amount})" : "";
                }
            }
        }

        public Item ModelItem {
            get {
                return modelitem;
            }
            set {
                SetProperty(ref modelitem, value);
            }
        }

        public string TradeMarket {
            get {
                return markettrade;
            }
            set {
                if (value.Length <= 4) {
                    value = string.Empty;
                }
                SetProperty(ref markettrade, value);
            }
        }

        public string ProductMarket {
            get {
                return marketproduct;
            }
            set {
                if (value.Length <= 4) {
                    value = string.Empty;
                }
                SetProperty(ref marketproduct, value);
            }
        }

        public event Action OnReload;
        public event Action<Item> OnSelected;

        private ObservableCollection<Market> datamarket;
        private ObservableCollection<Item> dataitem;
        private Market modelmarket;
        private Item modelitem;
        private string markettrade;
        private string marketproduct;

        private async Task SelectItemAsync() {
            await Task.Delay(0);
            try {
                OnSelected?.Invoke(ModelItem);
            } catch (Exception) {

            }

        }

        private async Task FinderItemAsync() {
            await Task.Run(() => {
                DataItem = new ObservableCollection<Item> {
                    new Item { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 1 },
                    new Item { Uid = 12, Name = "Red Elixir", Description = "Ramuan penyembuh luka hasil pertempuran, efek menambah 100 hp", Amount = 1 },
                    new Item { Uid = 13, Name = "Blue Powder", Description = "Memulihkan tenaga untuk kembali bertarung, efek menambah 100 sp", Amount = 1 },
                    new Item { Uid = 14, Name = "Green Leaf", Description = "Dapat digunakan untuk mencatat proses permainan", Amount = 1 },
                    new Item { Uid = 21, Name = "Gladius", Description = "Pedang kualitas tinggi, menambahkan 50 ATK dan mengaktifkan skill Force Attack", Amount = 1 },
                    new Item { Uid = 22, Name = "Great Sword", Description = "Pedang penghancur, menambahkan 100 ATK, 30 DEF dan mengaktifkan skill Splash Attack", Amount = 1 },
                    new Item { Uid = 31, Name = "Battle Gear", Description = "Armor besi, menambahkan 50 DEF, 100 HP, 50 SP dan mengaktifkan skill Battle Cry", Amount = 1 },
                    new Item { Uid = 41, Name = "Codex of Vitality", Description = "Instan efek menambah 50 maximum hp", Amount = 1 },
                    new Item { Uid = 42, Name = "Codex of Mentality", Description = "Instan efek menambah 50 maximum sp", Amount = 1 },
                };
            });
        }

        private async Task InitMarketAsync() {
            await Task.Run(() => {
                DataMarket = new ObservableCollection<Market> {
                    new Market { Uid = 1, Name = "Magic Stone Premium", Lock = 250, Price = 50, Trade = null, Product = { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 1 }, Slot = false },
                    new Market { Uid = 2, Name = "Green Leaf Premium", Lock = 250, Price = 100, Trade = null, Product = { Uid = 14, Name = "Green Leaf", Description = "Dapat digunakan untuk mencatat proses permainan", Amount = 1 }, Slot = false },
                    new Market { Uid = 3, Name = "High Grade Sword", Lock = 500, Price = 500, Trade = { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 20 }, Product = { Uid = 21, Name = "Gladius", Description = "Pedang kualitas tinggi, menambahkan 50 ATK dan mengaktifkan skill Force Attack", Amount = 1 }, Slot = false },
                    new Market { Uid = 4, Name = "Sword of Slayer", Lock = 1000, Price = 1500, Trade = { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 30 }, Product = { Uid = 22, Name = "Great Sword", Description = "Pedang penghancur, menambahkan 100 ATK, 30 DEF dan mengaktifkan skill Splash Attack", Amount = 1 }, Slot = false },
                    new Market { Uid = 5, Name = "The Battle Gear", Lock = 2000, Price = 2500, Trade = { Uid = 11, Name = "Magic Stone", Description = "Batu ini dapat digunakan untuk menajamkan kembali pedang,", Amount = 30 }, Product = { Uid = 31, Name = "Battle Gear", Description = "Armor besi, menambahkan 50 DEF, 100 HP, 50 SP dan mengaktifkan skill Battle Cry", Amount = 1 }, Slot = false },
                    new Market { Uid = 6, Name = "Codex of Vitality", Lock = 500, Price = 0, Trade = { Uid = 12, Name = "Red Elixir", Description = "Ramuan penyembuh luka hasil pertempuran, efek menambah 100 hp", Amount = 10 }, Product = { Uid = 41, Name = "Codex of Vitality", Description = "Instan efek menambah 50 maximum hp", Amount = 1 }, Slot = false },
                    new Market { Uid = 7, Name = "Codex of Mentality", Lock = 500, Price = 0, Trade = { Uid = 13, Name = "Blue Powder", Description = "Memulihkan tenaga untuk kembali bertarung, efek menambah 100 sp", Amount = 10 }, Product = { Uid = 41, Name = "Codex of Vitality", Description = "Instan efek menambah 50 maximum hp", Amount = 1 }, Slot = false },
                };
            });
        }

        private async Task ReadMarketAsync(bool asnew = false) {
            if (asnew) {
                await InitMarketAsync();
            } else {
                OnReload?.Invoke();
            }
        }

        private async Task<Market> ReadMarketAsync(int uid) {
            await Task.Delay(0);
            return DataMarket.Where(model => model.Uid.Equals(uid)).SingleOrDefault();
        }

        private async Task CreateMarketAsync() {
            DataMarket.Add(ModelMarket);
            await ReadMarketAsync();
        }

        private async Task UpdateMarketAsync() {
            var data = ModelMarket;
            DataMarket.Remove(ReadMarketAsync(data.Uid).Result);
            DataMarket.Add(data);
            await ReadMarketAsync();
        }

        private async Task DeleteMarketAsync() {
            DataMarket.Remove(ModelMarket);
            await ReadMarketAsync();
        }
    }
}
